import API from '../config/api';
import { infoActionsCreator } from "../store/actions/Info/InfoActionsCreators";
import { loadingActionsCreator } from "../store/actions/IsLoading/IsLoadingActionsCreators";
import { personajeActionsCreator } from "../store/actions/PersonajeActions/PersonajeActionsCreators";


const navigationMiddlewares = (url, dispatch) => {
    dispatch(loadingActionsCreator(true));
   
    API(url)
    .then(respJson => {
        console.log("respJSON ======");
        console.log(respJson);
        const info = respJson.info ? respJson.info : {};
        const results = respJson.results ? respJson.results : [];
        dispatch(infoActionsCreator(info));
        dispatch(personajeActionsCreator(results));
        dispatch(loadingActionsCreator(false));
    })
    .catch(error => {
        console.log(error);
        dispatch(loadingActionsCreator(false));
    })
}

export default navigationMiddlewares;