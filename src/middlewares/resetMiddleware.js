import API from '../config/api';
import { api, endpointAPI } from '../config';
import { infoActionsCreator } from "../store/actions/Info/InfoActionsCreators";
import { loadingActionsCreator } from "../store/actions/IsLoading/IsLoadingActionsCreators";
import { personajeActionsCreator } from "../store/actions/PersonajeActions/PersonajeActionsCreators";
import { resetActionsCreator } from '../store/actions/Reset/ResetActionsCreators';


const resetMiddleware = (dispatch) => {
    dispatch(loadingActionsCreator(true));
    dispatch(resetActionsCreator({}));
    const url = `${api}${endpointAPI.character}`;
    API(url)
    .then(respJson => {
        console.log("respJSON ======");
        console.log(respJson);
        const info = respJson.info ? respJson.info : {};
        const results = respJson.results ? respJson.results : [];
        dispatch(infoActionsCreator(info));
        dispatch(personajeActionsCreator(results));
        dispatch(loadingActionsCreator(false));
    })
    .catch(error => {
        console.log(error);
        dispatch(loadingActionsCreator(false));
    })
}

export default resetMiddleware;