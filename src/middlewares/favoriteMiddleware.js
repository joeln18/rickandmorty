import API from '../config/api';
import { loadingActionsCreator } from "../store/actions/IsLoading/IsLoadingActionsCreators";
import { personajeActionsCreator } from "../store/actions/PersonajeActions/PersonajeActionsCreators";


const favoriteMiddlewares = (url, dispatch) => {
    dispatch(loadingActionsCreator(true));
    
   
    API(url)
    .then(respJson => {
        console.log("favorite ===");
        console.log(respJson);
        dispatch(personajeActionsCreator(respJson));
        dispatch(loadingActionsCreator(false));
    })
    .catch(error => {
        console.log(error);
        dispatch(loadingActionsCreator(false));
    })
}

export default favoriteMiddlewares;