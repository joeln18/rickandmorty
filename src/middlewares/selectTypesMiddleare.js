import { api, endpointAPI } from "../config";
import API from '../config/api';
import { favoriteActionsCreator } from "../store/actions/Favorite/FavoriteActionsCreators";
import { infoActionsCreator } from "../store/actions/Info/InfoActionsCreators";
import { loadingActionsCreator } from "../store/actions/IsLoading/IsLoadingActionsCreators";
import { personajeActionsCreator } from "../store/actions/PersonajeActions/PersonajeActionsCreators";



const selectTypesMiddleare = (gender, status, name, dispatch) => {
    dispatch(loadingActionsCreator(true));
    dispatch(favoriteActionsCreator(false));
    let consult = '';
    if(gender != 'all' && status != 'all')
        consult = `/?gender=${gender}&status=${status}`;
    else if(gender == 'all' && status != 'all')
        consult = `/?status=${status}`;
    else if(status == 'all' && gender != 'all')
        consult = `/?gender=${gender}`;

    if(name != '' && consult != '') consult = consult + `&name=${name}`;
    else if(name != null) consult = `/?name=${name}`;;
    
    const url = `${api}${endpointAPI.character}${consult}`;
    console.log(url);
    API(url)
    .then(respJson => {
        console.log("respJSON ======");
        console.log(respJson);
        const info = respJson.info ? respJson.info : {};
        const results = respJson.results ? respJson.results : [];
        dispatch(infoActionsCreator(info));
        dispatch(personajeActionsCreator(results));
        dispatch(loadingActionsCreator(false));
    })
    .catch(error => {
        console.log("errror");
        console.log(error);
        dispatch(loadingActionsCreator(false));
    })
}

export default selectTypesMiddleare;