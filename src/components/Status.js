import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import selectTypesMiddleare from '../middlewares/selectTypesMiddleare';
import { statusActionsCreator } from '../store/actions/Status/StatusActionsCreators';


const Status = ({ innerWidth }) => {
    const { status, selectGender, name } = useSelector(state => state.persRed);
    const dispatch = useDispatch();
    const styleOption = {fontStyle: 'normal', fontWeight: 400, fontSize: innerWidth * 0.0097, marginRight: 10};
    //const [statusOption, setStatusOption] = useState("All");

    const changeOption = (event) => {
        const { target: { value = "" } = {} } = { ...event };
        dispatch(statusActionsCreator(value));

        selectTypesMiddleare(selectGender, value, name, dispatch);
        
    }

    return(
        <select 
            name="tipo_status"
            id="tipo_status"
            value={status}
            onChange={changeOption}
            style={{ ...styleOption, width: innerWidth * 0.1}}
        >
            <option value="all" style={styleOption}>Select Status</option>
            <option value="alive" style={styleOption}>Alive</option>
            <option value="dead" style={styleOption}>Dead</option>
            <option value="unknow" style={styleOption}>Unknow</option>
        </select>
    )
}

export default Status;