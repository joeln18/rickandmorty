import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import colors from '../constants/colors';
import resetMiddleware from '../middlewares/resetMiddleware';


const NoData = ({ innerWidth, innerHeight }) => {
    const dispatch = useDispatch();

    const handleReset = () => {
        console.log("reset");
        resetMiddleware(dispatch);
    }

    return(
        <div className="d-flex flex-column align-items-center justify-content-center" style={{
            width: innerWidth, 
            height: innerHeight * 0.55, 
            maxWidth: '98.4%',
            paddingLeft: innerWidth * 0.1395,
            paddingRight: innerWidth * 0.1507,
            paddingTop: 20,
            }}>
            
            <div>
                <h3 style={{fontStyle: 'normal', fontWeight: 700, fontSize: innerWidth * 0.025}}>
                    Uh-oh!
                </h3>
            </div>
            <div>
                <h3 style={{fontStyle: 'normal', fontWeight: 500, fontSize: innerWidth * 0.0138}}>
                    ¡Pareces perdido en tu viaje!
                </h3>
            </div>
            <div>
                <button
                    style={{
                        width: innerWidth * 0.108, 
                        height: innerHeight * 0.05, 
                        backgroundColor: colors.secondary, 
                        borderRadius: innerWidth * 0.041,
                        borderWidth: 0,
                        color: 'white',
                        fontSize: innerWidth * 0.01,
                        fontWeight: 700
                    }}
                    onClick={handleReset}
                >
                    Eliminar filtros
                </button>
            </div>
            
        </div>
    )
}

export default NoData;