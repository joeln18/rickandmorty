import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import colors from '../constants/colors';
import { api, endpointAPI  } from '../config';
import navigationMiddlewares from '../middlewares/navigationMiddleware';
import getRandomArbitrary from '../utils/randomArbitrary';
import { favoriteActionsCreator } from '../store/actions/Favorite/FavoriteActionsCreators';
import { resetActionsCreator } from '../store/actions/Reset/ResetActionsCreators';
import favoriteMiddlewares from '../middlewares/favoriteMiddleware';


const Favoritos = ({ innerWidth }) => {
    const { favoritePersonajes  } = useSelector(state => state.persRed);
    const dispatch = useDispatch();

    const handleFavorite = () => {
        dispatch(resetActionsCreator({}));
        dispatch(favoriteActionsCreator(!favoritePersonajes));
        let url = `${api}${endpointAPI.character}`;
        

        if(favoritePersonajes)
            navigationMiddlewares(url, dispatch)
        else{
            const number1 = getRandomArbitrary(1, 101);
            const number2 = getRandomArbitrary(1, 101);
            const number3 = getRandomArbitrary(1, 101);

            url = url + `/${number1},${number2},${number3}`;
            favoriteMiddlewares(url, dispatch);
        }

        
        
    }

    return(
        <div className="d-flex">
           <h3 style={{fontStyle: 'normal', fontWeight: 400, fontSize: innerWidth * 0.0097, marginRight: 10}}>Mostrar favoritos: </h3>
           <svg onClick={handleFavorite} xmlns="http://www.w3.org/2000/svg" width={ innerWidth * 0.0125} height={innerWidth * 0.0125} fill="currentColor" color={ favoritePersonajes ? colors.yellow : colors.grayTerc} className="bi bi-star-fill" viewBox="0 0 16 16">
                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
            </svg>
        </div>
    )
}

export default Favoritos;