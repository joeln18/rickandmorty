import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import overlayImg from '../assets/home/overlay.png';
import rickImg from '../assets/welcome/rickandmorty-trans.png';
import colors from '../constants/colors';
import text from '../constants/text';
import selectTypesMiddleare from '../middlewares/selectTypesMiddleare';
import { nameActionsCreator } from '../store/actions/Name/NameActionsCreators';

const Header = ({ innerWidth, innerHeight }) => {
    const dispatch = useDispatch();
    const { status, selectGender, name } = useSelector(state => state.persRed);
    
    const changeSearch = (event) => {
        const { target: { value = "" } = {} } = { ...event };
        console.log(value);
        dispatch(nameActionsCreator(value));
        selectTypesMiddleare(selectGender.toLowerCase(), status, value, dispatch);
    }

    return (
        <div style={{ width: innerWidth, height: innerHeight * 0.255}}>
            <img alt="overlayHeader" src={overlayImg} className="cover"
             style={{ 
                position: "absolute",
                width: '100%',
                height: innerHeight * 0.255,
                left: 0,
                top: 0,
                maxWidth: '100%'
             }} />
             <img alt="rickyHeader" src={rickImg} className="img-fluid"    
             style={{ 
                position: "absolute",
                width: '24%',
                height: innerHeight * 0.068,
                left: innerWidth * 0.378,
                top: innerHeight * 0.072
             }} />
             <div className="d-flex" 
                style={{
                    position: 'absolute', 
                    left: innerWidth * 0.326, 
                    width: innerWidth * 0.347, 
                    height: innerHeight * 0.034,
                    top: innerHeight * 0.149,
                    backgroundColor: colors.primary,
                    borderWidth: 2,
                    boxSizing: 'border-box',
                    borderColor: colors.gray,
                    padding: innerWidth * 0.005,
                    borderRadius: innerHeight * 0.003,
                    alignItems: 'center',
                    justifyContent: 'center'
                    }}>
                <svg xmlns="http://www.w3.org/2000/svg" width={innerWidth * 0.0194} height={innerWidth * 0.0194} fill="currentColor" color="white" className="bi bi-search" viewBox="0 0 16 16">
                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                </svg>
                <input 
                    type="search" placeholder={text.home.placeholder}
                    aria-label="Search"
                    style={{
                        width: '90%',
                        marginLeft: innerWidth * 0.0069,
                        height: '100%',
                        fontSize: innerWidth * 0.0125,
                        borderWidth: 0,
                        borderColor: 'transparent',
                        backgroundColor: colors.primary,
                        color: colors.white,
                        fontStyle: 'normal',
                        fontWeight: 'normal',
                        outline: 'none'
                    }}
                    onChange={changeSearch}
                    value={name}
                />
             
             </div>
        </div>
    )
}

export default Header;