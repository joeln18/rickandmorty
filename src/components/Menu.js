import React from 'react';
import colors from '../constants/colors';
import itemsMenu from '../constants/itemsMenu';
import ButtonMenu from './ButtonMenu';


const Menu = ({ innerWidth, innerHeight }) => {

    return(
        <div style={{
             width: innerWidth, 
             height: innerHeight * 0.0448, 
             backgroundColor: colors.gray, 
             maxWidth: '98.4%',
             paddingLeft: innerWidth * 0.1395,
             paddingRight: innerWidth * 0.1507
        }}>
           <div className="navbar" style={{height: '100%', justifyContent: 'space-between', padding: 0 }}>
               {
                   itemsMenu.map((item) => 
                    <ButtonMenu key={item.name} nameMenu={item.name} innerHeight={innerHeight} innerWidth={innerWidth} />
                   )
               }
           </div>
        </div>
    )
}

export default Menu;