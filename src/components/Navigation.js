import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import navigationMiddlewares from '../middlewares/navigationMiddleware';

//not-allowed
const Navigation = () => {
    const dispatch = useDispatch();
    const { info } = useSelector(state => state.persRed);
    const { next, prev } = info;

    const handleNext = () => navigationMiddlewares(next, dispatch);

    const handlePrevious = () => navigationMiddlewares(prev, dispatch);

    return(
        <nav aria-label="Navigation" className="mt-3">
            <ul className="pagination">
                <li className="page-item">
                    <a className="page-link" 
                        style={{cursor: prev != undefined && prev != null ? 'pointer' : 'not-allowed'}}
                        onClick={handlePrevious}
                        
                    >
                        Previous
                    </a>
                </li>
                <li className="page-item">
                    <a className="page-link" 
                        style={{cursor: next != undefined && next != null ? 'pointer' : 'not-allowed'}}
                        onClick={handleNext}
                        
                    >
                        Next
                    </a>
                </li>
            </ul>
        </nav>
    )
}

export default Navigation;