import React from 'react';
import { useSelector } from 'react-redux';
import CardPersonaje from './CardPersonaje';

const Personajes = ({ innerWidth, innerHeight }) => {
    const { personajes, isLoading } = useSelector(state => state.persRed);
    
    if(isLoading){
        return(
            <div className="d-flex justify-content-center align-items-center">
                <div className="container spinner-border" role="status">
                    <span className="visually-hidden">Loading...</span>
                    
                </div>
            </div>
        )
    }
    return(
        <div className="d-flex col-3 flex-wrap align-items-start" style={{
             width: innerWidth, 
             height: innerHeight * 0.55, 
             maxWidth: '98.4%',
             paddingLeft: innerWidth * 0.1395,
             paddingRight: innerWidth * 0.1507,
             paddingTop: 20,
             overflowY: 'scroll'
        }}>  
        
            {
                personajes.map(personaje => <CardPersonaje innerWidth={innerWidth} personaje={personaje} key={personaje.id} />)
            }
        </div>
    )
}

export default Personajes;