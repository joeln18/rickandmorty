import React from 'react';
import colors from '../constants/colors';


const CardPersonaje = ({ innerWidth,  personaje }) => {
    return(
        <div key={personaje.id} className="d-flex" style={{
            width: innerWidth * 0.22, 
            height: innerWidth * 0.097, 
            flexDirection: 'row',
            marginBottom: innerWidth * 0.015,
            marginRight: innerWidth * 0.006
        }}>
           
            <img 
                alt={personaje.name} 
                src={personaje.image} 
                width={innerWidth * 0.097} 
                height={innerWidth * 0.097}
                className="cover"
                style={{borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }}
            />
           <div
                style={{
                    width: innerWidth * 0.2263, 
                    height: innerWidth * 0.097, 
                    borderWidth: 1,
                    borderColor: colors.grayLigth,
                    borderStyle: 'solid',
                    borderEndEndRadius: 10,
                    borderTopRightRadius: 10,
                    paddingLeft: innerWidth * 0.01,
                    paddingTop: innerWidth * 0.011,
                    paddingBottom: innerWidth * 0.009
                }}
           >
                <div style={{flex: 1, height: innerWidth * 0.03}}>
                    <div className="d-flex" style={{height: innerWidth * 0.01}}>
                        <svg color={personaje.status == 'Alive' ? colors.green : colors.red } style={{marginTop: innerWidth * 0.001, marginRight: innerWidth * 0.002}} xmlns="http://www.w3.org/2000/svg" width={innerWidth * 0.005} height={innerWidth * 0.005} fill="currentColor" class="bi bi-circle-fill" viewBox="0 0 16 16">
                            <circle cx="8" cy="8" r="8"/>
                        </svg>
                        <h5 style={{fontStyle: 'normal', fontWeight: 'normal', fontSize: innerWidth * 0.0069, color: colors.graySecond}}>
                            { personaje.status } - { personaje.species }
                        </h5>
                    </div>
                    <div style={{height: innerWidth * 0.014}}>
                        <h5 style={{fontStyle: 'normal', fontWeight: 'normal', fontSize: innerWidth * 0.011, color: colors.black}}>
                            { personaje.name }
                        </h5>
                    </div>
                </div>
                <div style={{flex: 1, height: innerWidth * 0.03}}>
                    <div style={{height: innerWidth * 0.01}}>
                        <h5 style={{fontStyle: 'normal', fontWeight: 'normal', fontSize: innerWidth * 0.0069, color: colors.grayTerc}}>
                            Last known location:
                        </h5>
                    </div>
                    <div style={{height: innerWidth * 0.014}}>
                        <h5 style={{fontStyle: 'normal', fontWeight: 'normal', fontSize: innerWidth * 0.0083, color: colors.black}}>
                            { personaje.origin.name }
                        </h5>
                    </div>
                </div>
                <div style={{flex: 1, height: innerWidth * 0.03}}>
                    <div style={{height: innerWidth * 0.01}}>
                        <h5 style={{fontStyle: 'normal', fontWeight: 'normal', fontSize: innerWidth * 0.0069, color: colors.grayTerc }}>
                            First seen in:
                        </h5>
                    </div>
                    <div style={{height: innerWidth * 0.014 }}>
                        <h5 style={{fontStyle: 'normal', fontWeight: 'normal', fontSize: innerWidth * 0.0083, color: colors.black}}>
                            Never Ricking Morty
                        </h5>
                    </div>
                </div>
           </div>
        </div>
    )
}

export default CardPersonaje;