import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectGenderActions } from '../store/actions/selectGender/SelectGenderActions';
import selectTypesMiddleare from '../middlewares/selectTypesMiddleare';


const ButtonMenu = ({nameMenu, innerHeight, innerWidth}) => {
    const { status, selectGender, name } = useSelector(state => state.persRed);

    const dispatch = useDispatch();

    const buttonStyle = { 
        display: 'grid', 
        width: '100%', 
        height: innerHeight * 0.0448, 
        alignItems: 'center', 
        fontStyle: 'normal', 
        fontSize: innerWidth * 0.0125, 
        fontWeight: 400, 
        cursor: 'pointer'
     };

    const selectionItem = (event) => {
        
        const selectGender = event.target.innerText;
        const lowerGender = selectGender.toLowerCase();
        dispatch(selectGenderActions(lowerGender));

        selectTypesMiddleare(lowerGender, status, name, dispatch);
    
    }

    return(
        <div key={name} style={{
            width: innerWidth * 0.1123, 
            textAlign: 'center',
            height: '100%'
        }}>
            <span 
                className={`border-bottom ${selectGender == nameMenu.toLowerCase() ? 'border-success' : ''}`}
                style={buttonStyle}
                onClick={selectionItem}
            >
                { nameMenu }
            </span>
        </div>
    )
}

export default ButtonMenu;