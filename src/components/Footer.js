import React, { useState, useEffect } from 'react';
import overlayImg from '../assets/home/portal.png';
import suazoImg from '../assets/welcome/suazo-trans.png';


const Footer = ({ innerWidth, innerHeight }) => {

    
    return(
        <div className="container">
            <img alt="overlayHeader" src={overlayImg} className="img-fluid"
             style={{ 
                position: "absolute",
                width: innerWidth,
                height: innerHeight * 0.079,
                left: 0,
                bottom: 0,
                maxWidth: '100%',
                top: innerHeight * 0.92
             }} />
             <img src={suazoImg} className="img-fluid"    
             style={{ 
                position: "absolute",
                width: innerWidth * 0.093,
                height: innerHeight * 0.028,
                left: innerWidth * 0.458,
                top: innerHeight * 0.946
             }} />
        </div>
    )
}

export default Footer;