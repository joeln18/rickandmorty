import React from 'react';
import { useSelector } from 'react-redux';
import Favoritos from './Favoritos';
import Navigation from './Navigation';
import Status from './Status';



const Options = ({ innerWidth, innerHeight }) => {
    const { info } = useSelector(state => state.persRed);
    return(
        <div className="d-flex align-items-center justify-content-between" style={{
            width: innerWidth, 
            height: innerHeight * 0.05, 
            maxWidth: '98.4%',
            paddingLeft: innerWidth * 0.1395,
            paddingRight: innerWidth * 0.1507,
            paddingTop: 20,
            }}>
            <Favoritos  innerWidth={ innerWidth} />
            <Status innerWidth={ innerWidth} />
            {
                info != undefined && info != null && (
                    <Navigation innerWidth={ innerWidth} />
                )
            }
            
        </div>
    )
}

export default Options;