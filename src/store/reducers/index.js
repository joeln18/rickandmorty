import { combineReducers } from "redux";
import PersonajeReducers from './PersonajeReducers';

export default combineReducers({
    persRed: PersonajeReducers
});