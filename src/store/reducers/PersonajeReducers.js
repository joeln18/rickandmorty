import { 
    GET_PERSONAJE, 
    SELECT_GENDER, 
    SELECT_PERSONAJE, 
    FAVORITE_PERSONAJES, 
    IS_LOADING,
    STATUS, 
    INFO,
    NAME,
    RESET
 } from '../types/typesActions';

const initialState =  {
    personajes: [],
    selectGender: 'All',
    favoritePersonajes: false,
    selectPersonaje: {},
    isLoading: false,
    status: 'all',
    info: {},
    name: ''
}


export default function(state= initialState, action){
    switch (action.type) {
        case GET_PERSONAJE:
            return{
                ...state,
                personajes: action.payload
            }

        case SELECT_GENDER:
            return{
                ...state,
                selectGender: action.payload
            }
        
        case SELECT_PERSONAJE:
            return{
                ...state,
                selectPersonaje: action.payload
            }

        case FAVORITE_PERSONAJES:
            return{
                ...state,
                favoritePersonajes: action.payload
            }
        case IS_LOADING:
            return{
                ...state,
                isLoading: action.payload
            }
        case STATUS:
            return{
                ...state,
                status: action.payload
            }
        case INFO:
            return{
                ...state,
                info: action.payload
            }
        case NAME:
            return{
                ...state,
                name: action.payload
            }
        case RESET:
            return{
                ...initialState
            }
        default:
            return state;
    }
}