import { resetActions } from "./ResetActions";


export const resetActionsCreator = (info) => async (dispatch) => {
    try{
        dispatch(resetActions(info));
    }catch(error){
        console.log(error);
    }
}