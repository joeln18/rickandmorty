import { RESET } from "../../types/typesActions";


export const  resetActions = (state) =>  ({
    type: RESET,
    payload: state
});