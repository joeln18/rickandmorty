import { INFO } from "../../types/typesActions";


export const  infoActions = (info) =>  ({
    type: INFO,
    payload: info
});