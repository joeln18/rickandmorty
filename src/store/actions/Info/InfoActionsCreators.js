import { infoActions } from "./InfoActions";


export const infoActionsCreator = (info) => async (dispatch) => {
    try{
        dispatch(infoActions(info));
    }catch(error){
        console.log(error);
    }
}