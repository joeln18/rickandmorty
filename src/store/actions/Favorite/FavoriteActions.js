import { FAVORITE_PERSONAJES } from "../../types/typesActions";


export const  favoriteActions = (favorite) =>  ({
    type: FAVORITE_PERSONAJES,
    payload: favorite
});