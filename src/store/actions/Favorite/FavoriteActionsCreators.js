import { favoriteActions } from "./FavoriteActions";


export const favoriteActionsCreator = (favorite) => async (dispatch) => {
    try{
        dispatch(favoriteActions(favorite));
    }catch(error){
        console.log(error);
    }
}