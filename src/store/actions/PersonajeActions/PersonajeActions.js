import { GET_PERSONAJE } from "../../types/typesActions";


export const  personajeActions = (personajes) =>  ({
    type: GET_PERSONAJE,
    payload: personajes
});