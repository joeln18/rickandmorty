import { personajeActions } from "./PersonajeActions";


export const personajeActionsCreator = (personajes) => async (dispatch) => {
    try{
        dispatch(personajeActions(personajes));
    }catch(error){
        console.log(error);
    }
}