import { nameActions } from "./NameActions";


export const nameActionsCreator = (name) => async (dispatch) => {
    try{
        dispatch(nameActions(name));
    }catch(error){
        console.log(error);
    }
}