import { NAME } from "../../types/typesActions";


export const  nameActions = (name) =>  ({
    type: NAME,
    payload: name
});