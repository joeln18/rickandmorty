import { STATUS } from "../../types/typesActions";


export const  statusActions = (status) =>  ({
    type: STATUS,
    payload: status
});