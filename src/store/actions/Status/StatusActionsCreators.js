import { statusActions } from "./StatusActions";


export const statusActionsCreator = (status) => async (dispatch) => {
    try{
        dispatch(statusActions(status));
    }catch(error){
        console.log(error);
    }
}