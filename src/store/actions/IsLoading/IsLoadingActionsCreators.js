import { loadingActions } from "./IsLoadingActions";


export const loadingActionsCreator = (loading) => async (dispatch) => {
    try{
        dispatch(loadingActions(loading));
    }catch(error){
        console.log(error);
    }
}