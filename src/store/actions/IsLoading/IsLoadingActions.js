import { IS_LOADING } from "../../types/typesActions";


export const  loadingActions = (loading) =>  ({
    type: IS_LOADING,
    payload: loading
});