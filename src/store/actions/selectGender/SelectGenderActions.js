import { SELECT_GENDER } from "../../types/typesActions";


export const  selectGenderActions = (gender) =>  ({
    type: SELECT_GENDER,
    payload: gender
});