import { selectGenderActions } from "./SelectGenderActions";


export const selectGenderActionsCreator = (gender) => async (dispatch) => {
    try{
        dispatch(selectGenderActions(gender));
    }catch(error){
        console.log(error);
    }
}