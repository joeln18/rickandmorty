import { createStore, applyMiddleware, compose } from "redux";
import reducers from "./reducers";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import thunk from 'redux-thunk';
import { logger } from 'redux-logger';



const persistConfig = {
  key: "root",
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducers); // create a persisted reducer

const store = createStore(
    persistedReducer,
    compose(
      applyMiddleware(thunk, logger),
      typeof window == "object" &&
        typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== "undefined"
        ? window.__REDUX_DEVTOOLS_EXTENSION__()
        : (f) => f
    )
  );

const persistor = persistStore(store); // used to create the persisted store, persistor will be used in the next step

export { store, persistor };