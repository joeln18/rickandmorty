export const GET_PERSONAJE = 'GET_PERSONAJE';
export const SELECT_PERSONAJE = 'SELECT_PERSONAJE';
export const SELECT_GENDER = 'SELECT_GENDER';
export const FAVORITE_PERSONAJES = 'FAVORITE_PERSONAJES';
export const IS_LOADING = 'IS_LOADING';
export const STATUS = 'STATUS';
export const INFO = 'INFO';
export const NAME = 'NAME';
export const RESET = 'RESET';

