import React, { lazy, Suspense } from "react";
import { Switch, Route } from "react-router-dom";
import { AnimatePresence, motion } from "framer-motion";


const baseLazy = (dir) => lazy(() => import(`${dir}`));

const routes = [
    {
        path: '/',
        exact: true,
        view: baseLazy('./views/Welcome')
    },
    {
        path: '/home/',
        exact: true,
        view: baseLazy('./views/Home')
    }
];

const SuspenseLoading = () => {
    return(
        <>
            <div>
                <p>cargando....</p>
            </div>
        </>
    )
}


const Routes = () => {

    return(
        <AnimatePresence>
            <Suspense fallback={<SuspenseLoading />}>
                <Switch>
                    {
                        routes.map((route, indice) => 
                            <Route 
                                key={indice}
                                path={route.path} 
                                exact={route.exact} 
                                component={route.view}
                            />
                        )
                    }
                </Switch>
            </Suspense>
        </AnimatePresence>
    )
}

export default Routes;


