import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Menu from '../components/Menu';
import NoData from '../components/NoData';
import Options from '../components/Options';
import Personajes from '../components/Personajes';
import resetMiddleware from '../middlewares/resetMiddleware';

const Home = () => {
    const [innerWidth, setInnerWidth] = useState(window.innerWidth);
    const [innerHeight, setInnerHeight] = useState(window.innerWidth * 0.883);
    const { personajes, status, selectGender } = useSelector(state => state.persRed);
    const dispatch = useDispatch();
    

    useEffect(() => {
        window.addEventListener("resize", resizeWindow);
        initData();
        
        return () => {
            window.removeEventListener("resize", resizeWindow);
        }
    }, []);

   const resizeWindow = () => {
      setInnerWidth(window.innerWidth);
      setInnerHeight(window.innerWidth * 0.883);
   }

   const initData = () => {
       if(personajes.length == 0 && status == 'all' && selectGender.toLowerCase() == 'all')
        resetMiddleware(dispatch);
   }

    return (
       <div style={{width: innerWidth, height: innerHeight }}>
        <Header innerWidth={innerWidth} innerHeight={innerHeight} />
        <Menu innerWidth={innerWidth} innerHeight={innerHeight} />
        <Footer innerWidth={innerWidth} innerHeight={innerHeight} />
        <Options innerWidth={innerWidth} innerHeight={innerHeight} />
        {
            personajes.length > 0 ? (
                <Personajes innerWidth={innerWidth} innerHeight={innerHeight} />
            ):
            (
                <NoData innerWidth={innerWidth} innerHeight={innerHeight} />
            )
        }
        

       </div>
    )
}

export default Home;
