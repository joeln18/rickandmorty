import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import portalImg from '../assets/welcome/portal-morty-smith-rick.png';
import rickImg from '../assets/welcome/rickandmorty-trans.png';
import suazoImg from '../assets/welcome/suazo-trans.png';
import colors from '../constants/colors';
import text from '../constants/text';


const Welcome = () => {
   const { title, description } = text.welcome;
   const [innerWidth, setInnerWidth] = useState(window.innerWidth);
   const [innerHeight, setInnerHeight] = useState(window.innerWidth * 0.7);

   useEffect(() => {
      window.addEventListener("resize", resizeWindow);
  
      return () => {
        window.removeEventListener("resize", resizeWindow);
      }
    }, []);

   const resizeWindow = () => {
      setInnerWidth(window.innerWidth);
      setInnerHeight(window.innerWidth * 0.7);
   }

    return (
        <div className="container">
           <img src={portalImg} className="img-fluid"
             style={{ 
                position: "absolute",
                width: innerWidth,
                height: innerHeight,
                left: 0,
                top: 0,
                maxWidth: '100%'
             }} />

            <img src={suazoImg} className="img-fluid"    
             style={{ 
                position: "absolute",
                width: innerWidth * 0.093,
                height: innerHeight * 0.0489,
                left: innerWidth * 0.453,
                top: innerHeight * 0.291
             }} />
            <img src={rickImg} className="img-fluid"    
             style={{ 
                position: "absolute",
                width: innerWidth * 0.461,
                height: innerHeight * 0.227,
                left: innerWidth * 0.29,
                top: innerHeight * 0.339
             }} />
             <div style={{
                width: innerWidth, 
                position: 'absolute', 
                top: innerHeight * 0.5869, 
                left: 0
                }}>
                  <h1 
                  style={{
                     color: 'white', 
                     fontSize: innerWidth * 0.025,
                     textAlign: 'center',
                     fontWeight: 'normal',
                     fontStyle: 'normal'
                     }}>{ title}
                     </h1> 
             </div>
             <div style={{
                width: innerWidth * 0.605, 
                position: 'absolute', 
                top: innerHeight * 0.6484, 
                left: innerWidth * 0.1972, 
                height: innerHeight * 0.06 
                }}>
                  <p
                  style={{
                     color: 'white', 
                     textAlign: 'center',
                     fontWeight: 'normal',
                     fontSize: innerWidth * 0.0125,
                     fontStyle: 'nomal'
                     }}>
                        { description }
                     </p> 
             </div>
             <Link to="/home">
               <button
                  style={{
                     position: 'absolute', 
                     width: innerWidth * 0.086, 
                     height: innerHeight * 0.05, 
                     left: innerWidth * 0.458, 
                     top: innerHeight * 0.748, 
                     backgroundColor: colors.secondary, 
                     borderRadius: innerWidth * 0.041,
                     borderWidth: 0,
                     color: 'white',
                     fontSize: innerWidth * 0.01,
                     fontWeight: 700
                  }}
               >
                  Continuar
               </button>
             </Link>
            
        </div>
    )
}

export default Welcome;