

export const api = 'https://rickandmortyapi.com/api';

export const endpointAPI = {
    character: '/character',
    location: '/location',
    episode: '/episode'
}