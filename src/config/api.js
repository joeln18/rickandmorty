

const api = async (url) => {
    const query = await  fetch(url,{
        method: 'GET'
    })
    .then(resp => resp.json())
    
    return query;
}

export default api;