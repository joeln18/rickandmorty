export default {
    secondary: '#11555F',
    primary: '#081F32',
    white: '#FFFFFF',
    gray: '#F2F2F2',
    grayLigth: '#E0E0E0',
    green: '#27AE60',
    graySecond: '#4F4F4F',
    black: '#000000',
    grayTerc: '#828282',
    yellow: '#F2994A',
    red: '#EB5757'
}