export default  [
    {
        id: '1',
        name: 'All'
    },
    {
        id: '2',
        name: 'Unknow'
    },
    {
        id: '3',
        name: 'Female'
    },
    {
        id: '4',
        name: 'Male'
    },
    {
        id: '5',
        name: 'Genderless'
    },
];