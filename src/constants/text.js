export default {
    welcome: {
        title: 'Bienvenido a Rick and Morty',
        description: 'En esta prueba, evaluaremos su capacidad para construir la aplicación mediante el análisis de código y la reproducción del siguiente diseño.',
    },
    home:{
        placeholder: 'Buscar personaje...'
    }
}